# INFO #

A classic Snake game implementation in Java using Processing libraries (https://processing.org/tutorials/eclipse/).
![had.png](https://bitbucket.org/repo/oB76oy/images/316875926-had.png)
### Requirements ###
To compile the code, you need JDK1.6+

### How to setup ###

Download and unzip the code. The project was created in IntelliJ IDEA, you can open it there directly and run the GameCanvas class.

You can also compile and run it directly from the command-line like this:

In the unzipped folder, create a folder named "bin".
Compile the code using the command:

```
javac -cp ".:bin:lib/*" -d bin ./src/game/*.java
```
Now run it using:

```
java -cp ".:bin:lib/*" game.GameCanvas
```

(Note that the colon character ':' in the classpath is a system specific class separator and might be different depending on your system)