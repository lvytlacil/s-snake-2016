package game;

import processing.core.PImage;

/**
 * Represents food the snake can eat. Food can have different score value and effect.
 * @author Libor
 *
 */
public class Apple extends GameObject {

	private PImage imgApple;
	private int x;
	private int y;
	private FoodType type;
	//private GameWorld gameWorld;
	
	/**
	 * Creates a new food instance of the given type.
	 * @param gameWorld The gameworld the apple will be attached to
	 * @param type Type of the food
	 */
	private Apple(GameWorld gameWorld, FoodType type) {
		super(gameWorld);
		imgApple = gameWorld.getAsset("apple");
		this.gameWorld = gameWorld;
		setType(type);
	}
	
	/**
	 * Sets the type of this food causing different image to be drawn for this food
	 * and possibly changing its value and effect.
	 * @param type The type to be set
	 */
	public void setType(FoodType type) {
		switch (type) {
		case APPLE:
			imgApple = gameWorld.getAsset("apple");
			break;
		case CHERRY:
			imgApple = gameWorld.getAsset("cherry");
		}
		this.type = type;
	}
	
	/**
	 * Moves the food to the given location.
	 * @param x Target x position
	 * @param y Target y position
	 */
	public void place(int x, int y) {
		assert(x >= 0 && x < GameWorld.X_SIZE && y >= 0 && y < GameWorld.Y_SIZE);
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Gets the x coordinate of this object.
	 * @return
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Gets the y coordinate of this object.
	 * @return
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Gets the score value of this food.
	 * @return
	 */
	public int getValue() {
		switch (type) {
		case APPLE:
			return 50;
		case CHERRY:
			return 0;
		default:
			return 0;
		}
	}
	
	/**
	 * Gets the effect of this food.
	 * @return
	 */
	public Effect getEffect() {
		switch (type) {
		case APPLE:
			return Effect.GROW;
		case CHERRY:
			return Effect.SHRINK;
		default:
			return Effect.GROW;
		}
	}
	
	
	/**
	 * Creates and initializes an apple and moves it to specified position.
	 * @param gameWorld
	 * @return
	 */
	public static Apple create(GameWorld gameWorld, FoodType type, int x, int y) {
		Apple result = new Apple(gameWorld, type);
		result.init();
		result.place(x, y);
		return result;
	}

	/* GameObject implementation */
	
	@Override
	public boolean isAlive() {
		return super.isAlive();
	}
	
	@Override
	public void die() {
		super.die();
	}
	
	@Override
	public void init() {
		super.init();
	}

	@Override
	public void update(long lastUpdateMillis) {
		super.update(lastUpdateMillis);
	}

	@Override
	public void draw() {
		super.draw();
		gameWorld.drawImage(imgApple, x, y, true);		
	}
	
	enum FoodType {APPLE, CHERRY}
	enum Effect {
		GROW, SHRINK, NONE
	}
}
