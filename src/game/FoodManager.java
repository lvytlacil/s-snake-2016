package game;

import java.util.HashSet;
import java.util.Stack;

import game.Apple.FoodType;

/**
 * Represents a class that manages food within the gameworld
 */
public class FoodManager extends GameObject {
	/* Public constants */
	/* You can change these values to directly affect the gameplay */
	private static final int MAX_FOOD = 3;
	private static final long SPAWN_TIME_MILLIS = 2000;
    // How many times an apple must spawn before a cherry spawns.
	private static final int SPAWNS_TO_CHERRY = 12;
	
	/* Private data fields */
	// Underlying collection for storing and reusing food objects
	private HashSet<Apple> foodData = new HashSet<>();
	private Stack<Apple> foodToReuse = new Stack<>();
	
	// Time between spawns
	private long lastSpawnTime;
	// If spawning should be paused
	private boolean paused;
	// Total food spawned
	private int spawnedTotal;
	
	public FoodManager(GameWorld gameWorld) {
		super(gameWorld);
	}
	
	/* Private and helper methods */
	
	/* Returns a position in the gameword, that is not occupied by snake or a food */
	private IntPair getRandomFreePosition(GameWorld gameWorld) {
		return gameWorld.getRandomFreePosition();
	}
	
	/* This method returns true iff there are less than MAX_FOOD spawned */
	private boolean isLessThanMax() {
		return !foodToReuse.empty() || foodData.size() < MAX_FOOD;
	}
	
	/* Spawns a new food assuming there is room for it */
	private void insertFood(int x, int y) {
		assert(isLessThanMax());
		// Is it apple or cherry?
		FoodType type = (spawnedTotal > 0 && spawnedTotal % SPAWNS_TO_CHERRY == 0)
				? FoodType.CHERRY : FoodType.APPLE;
		// Determine whether we reuse existing object or need to create a new one
		if (foodData.size() < MAX_FOOD) {			
			Apple apple = Apple.create(gameWorld, type, x, y);
			foodData.add(apple);
			lastSpawnTime = 0;
		} else if (!foodToReuse.empty()) {
			Apple apple = foodToReuse.pop();
			apple.setType(type);
			apple.init();
			apple.place(x, y);			
			lastSpawnTime = 0;
		}	
		spawnedTotal++;
	}
	
	/**
	 * Determines whether there is a food on the given position. Complexity is O(n) where
	 * n is the maximum number of food that can appear on the screen, i.e positions are not cached unlike
	 * the pieces of snake, because there is not supposed to be a lot of food on the screen at once.
	 * @param x The x position to be checked
	 * @param y The y position to be checked
	 * @return true if the position is occupied, false otherwise
	 */
	public boolean isOnGridPosition(int x, int y) {
		for (Apple apple : foodData) {
			if (apple.isAlive()) {
				if (x == apple.getX() && y == apple.getY())
					return true;
			}
		}
		return false;
	}
	
	/**
	 * If there is an apple on the given coordinates, it dies and it is returned. If there is
	 * no food on the given position, null is returned instead.
	 * @param x The x position to be checked
	 * @param y The y position to be checked
	 */
	public Apple eatAtPosition(int x, int y) {
		for (Apple apple : foodData) {
			if (apple.isAlive()) {
				if (x == apple.getX() && y == apple.getY()) {
					apple.die();
					// Add for reuse
					foodToReuse.push(apple);
					return apple;
				}
			}
		}
		return null;
	}
	
	/**
	 * Starts spawning food again.
	 */
	public void resume() {
		paused = false;
	}
	
	/**
	 * Pauses food spawning.
	 */
	public void pause() {
		paused = true;
	}
	
	/* GameObject implementation */

	@Override
	public void die() {
		for (Apple apple : foodData) {
			if (!apple.isAlive())
				apple.die();
		}
		super.die();
	}

	public void init(GameWorld gameWorld) {
		super.init();
		foodData.clear();
		foodToReuse.clear();
		lastSpawnTime = 0;
		spawnedTotal = 0;
		// After init, the manager will be paused by default.
		paused = true;
	}


	public void update(GameWorld gameWorld, long lastUpdateMillis) {
		super.update(lastUpdateMillis);
		
		if (paused)
			return;
		lastSpawnTime += lastUpdateMillis;
		// Every SPAWN_TIME_MILLIS, try spawning new food
		if (lastSpawnTime > SPAWN_TIME_MILLIS) {
			lastSpawnTime = 0;
			if (isLessThanMax()) {
				//dump();
				IntPair coords = getRandomFreePosition(gameWorld);
				if (coords != null) {
					insertFood(coords.x, coords.y);		
				}
			}
		}
		
		// Update food that is alive
		for (GameObject food : foodData) {
			if (food.isAlive())
				food.update(lastUpdateMillis);
		}		
	}

	
	public void draw() {
		super.draw();
		// Draw food that is alive
		for (GameObject food : foodData) {
			if (food.isAlive())
				food.draw();
		}
	}
	
	/* Methods for testing */
	public void dump() {
		StringBuilder sb = new StringBuilder();
		sb.append("Stored apples: ").append(foodData);
		sb.append("; Objects to reuse: ").append(foodToReuse);
		System.err.println(sb.toString());
	}


}
