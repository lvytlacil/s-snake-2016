package game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.function.Consumer;

import processing.core.PImage;

/**
 * The game object that represents the player
 */
public class Snake extends GameObject {
	/* Constants */
	// Constants related to speed
	private static final int START_MILLIS_TO_MOVE = 450; // Init speed. The lower, the faster.
	private static final int MIN_MILLIS_TO_MOVE = 150; // Final speed. The lower, the faster
	private static final int SPEED_UP_FACTOR = 25;
	
	private static final Direction INITIAL_DIRECTION = Direction.RIGHT;
	private static final int INITIAL_X_COORD = 5;
	private static final int INITIAL_Y_COORD = 5;
	private static final int INITIAL_LENGTH = 3;
	// Maps snake's direction to its opposite, so that we can forbid changing its direction
	// directly to its opposite.
	private static final HashMap<Direction, Direction> OPPOSITE_DIRECTION_MAP;
	
	static {
		OPPOSITE_DIRECTION_MAP = new HashMap<>();
		OPPOSITE_DIRECTION_MAP.put(Direction.LEFT, Direction.RIGHT);
		OPPOSITE_DIRECTION_MAP.put(Direction.RIGHT, Direction.LEFT);
		OPPOSITE_DIRECTION_MAP.put(Direction.UP, Direction.DOWN);
		OPPOSITE_DIRECTION_MAP.put(Direction.DOWN, Direction.UP);
	}
	
	/* Private fields */
	/*== Fields representing state that changes troughout the game */
	// Representation of snake's body
	private LinkedList<Body> elemsQu;
	// The movement direction and the direction it should be changed to during the next update
	private Direction direction, changedDirection;
	// The current speed
	private int millisToMove;
	// Time in milliseconds since last state update
	private int lastUpdateMillis;
	// Represents how many cells the snake should grow over the next updates
	// before returning to normal
	private int cellsToNormal;
	// Total steps taken
	private long steps;
	// Collection of registered actions for updating
	private ArrayList<Consumer<UpdateEventClass>> updateActionList;
	
	/*== Fields representing state that remains the same the whole game, like assets etc. */
	private PImage imgHead;
	private PImage imgRest;
	
	/* Private methods */
	
	/*== Lower level methods for accessing and modifying the underlying collection of snake's body */
	
	/* Gets the first element, that is the element that has been queued last (the oldest element, first
	 * in the queue). It corresponds to the tail of the snake.
	 */
	private Body getFirst() {
		return elemsQu.getFirst();
	}
	
	/* Gets the last element, that is the element that has been queued first (the youngest element,
	 * last in the queue. It corresponds to the head of the snake.
	 */
	private Body getLast() {
		return elemsQu.getLast();
	}
	
	/* Enqueues given element. This element becomes the youngest elemenent in the queue */
	private void addLast(Body body) {
		elemsQu.addLast(body);
	}
	
	/* Dequeues an element (the oldest in the queue) and returns it. */
	private Body removeFirst() {
		assert(elemsQu.size() > 0);
		Body result = elemsQu.removeFirst();
		return result;
	}
	
	/* Gets the size of the underlying collection */
	private int size() {
		return elemsQu.size();
	}
	
	/* Removes all elements from the underlying collection */
	private void clear() {
		elemsQu.clear();
	}
	
	/*==  Helper methods and methods that are extracted from the public interface */
	
	/* Simulates the movement of the snake in the current direction by removing the tail of the snake
	 * (oldest element in the queue) and placing it at a new position as a new head. The rest of the elements
	 * remain unchanged.
	 */
	private void tailToFront() {
		Body oldTail = removeFirst();
		Body head = getLast();
		
		assert(!elemsQu.contains(oldTail));
		
		switch (direction) {
		case LEFT:
			oldTail.x = fitCoords(head.x - 1, true);
			oldTail.y = head.y;
			break;
		case RIGHT:
			oldTail.x = fitCoords(head.x + 1, true);
			oldTail.y = head.y;
			break;
		case UP:
			oldTail.x = head.x;
			oldTail.y = fitCoords(head.y - 1, false);
			break;
		case DOWN:
			oldTail.x = head.x;
			oldTail.y = fitCoords(head.y + 1, false);
			break;
		default:
			assert(false);			
		}
		
		/* Note that oldTail object is updated and reused. No new object gets created */
		addLast(oldTail);
	}
	
	/* Make given coordinates fit inside the parentWorld in a way, so that
	 * the snake can pass through a wall and appear on the other side as usual.
	 */
	private int fitCoords(int val, boolean xaxis) {
		int bound = xaxis ? GameWorld.X_SIZE : GameWorld.Y_SIZE;
		if (val < 0)
			return bound - 1;
		else if (val >= bound)
			return 0;
		else
			return val;
	}
	
	/* Grows the snake by one */
	private void grow() {
		assert(size() > 0);
		Body head = getLast();
		int x = head.x;
		int y = head.y;
		switch (direction) {
		case LEFT:
			x = fitCoords(x - 1, true);
			break;
		case RIGHT:
			x = fitCoords(x + 1, true);
			break;
		case UP:
			y = fitCoords(y - 1, false);
			break;
		case DOWN:
			y = fitCoords(y + 1, false);
			break;
		}
		
		assert(x >= 0 && x < GameWorld.X_SIZE && y >= 0 && y < GameWorld.Y_SIZE);
		addLast(new Body(x, y));
	}
	
	public Snake(GameWorld parentWorld) {
		super(parentWorld);
		
		this.elemsQu = new LinkedList<>();
		this.updateActionList = new ArrayList<>();
		// Setup assets
		imgHead = parentWorld.getAsset("snakeHead");
		imgRest = parentWorld.getAsset("snakeRest");
		assert(imgHead != null && imgRest != null);
	}
	
	/* Snake interaction */
	/**
	 * Changes the snake's direction
	 * @param direction
	 */
	public void changeDirection(Direction direction) {
		assert(OPPOSITE_DIRECTION_MAP.containsKey(direction));
		if (direction != OPPOSITE_DIRECTION_MAP.get(this.direction))
		this.changedDirection = direction;
	}
	
	/**
	 * Causes the snake to eat and grow over the next update calls.
	 * @param cells The number of cells the snake should grow. This is the number of update calls
	 * required to finish growing.
	 */
	public void eat(int cells) {
		cellsToNormal += cells;
	}
	
	/**
	 * Causes the snake to shrink immedieately by the given number of cells.
	 * @param cells The number of cells to remove from snake. However, the snake will never shrink
	 * past its initial length.
	 */
	public void shrink(int cells) {
		assert(size() >= INITIAL_LENGTH);
		// How many cells is about to be removed. If the snake is growing, the number of
		// cells left to grow will be substracted.
		int diff = cells - cellsToNormal;		
		for (int i = 0; i < diff; i++) {
			if (size() == INITIAL_LENGTH)
				break;
			int oldTailX = getTailX();
			int oldTailY = getTailY();
			removeFirst();
			for (Consumer<UpdateEventClass> action : updateActionList) {
				action.accept(new UpdateEventClass(this, oldTailX, oldTailY, false, true));
			}
		}
		cellsToNormal = 0;
	}
	
	/**
	 * Gets the x-coordinate of the head.
	 * @return
	 */
	public int getHeadX() {
		return getLast().x;
	}
	
	/**
	 * Gets the y-coordinate of the head.
	 * @return
	 */
	public int getHeadY() {
		return getLast().y;
	}
	
	/**
	 * Gets the x-coordinate of the tail
	 * @return
	 */
	public int getTailX() {
		return getFirst().x;
	}
	
	/**
	 * Gets the y-coordinate of the tail.
	 * @return
	 */
	public int getTailY() {
		return getFirst().y;
	}
	
	/**
	 * Returns the total number of the snake has taken already.
	 * @return
	 */
	public long getStepsTaken() {
		return steps;
	}
	
	/**
	 * Performs an action over every cell of snake's body.
	 * @param action
	 */
	public void actionOverCells(Consumer<Body> action) {
		for (Body cell : elemsQu) {
			action.accept(cell);
		}
	}
	
	/**
	 * Performs an action over every cell of snake's body that is not the head.
	 * @param action
	 */
	public void actionOverBody(Consumer<Body> action) {
		Iterator<Body> it = elemsQu.iterator();
		for (int i = 0; i < elemsQu.size() - 1; i++) {
			action.accept(it.next());
		}
	}
	
	/**
	 * Speeds the snake up. The snake can never become faster than its maximum.
	 * allowed speed though.
	 */
	public void speedUp() {
		millisToMove = millisToMove - SPEED_UP_FACTOR;
		if (millisToMove < MIN_MILLIS_TO_MOVE)
			millisToMove = MIN_MILLIS_TO_MOVE;
	}
	
	public void slowDown() {
		
	}
	
	/* Event registration */
	
	/**
	 * Allows to register an action to be called when the snake gets updated.
	 * @param action
	 */
	public void registerForUpdate(Consumer<UpdateEventClass> action) {
		if (!updateActionList.contains(action))
			updateActionList.add(action);
	}
	
	/**
	 * Allows to unregister an action to be called when the snake gets updated.
	 * @param action
	 */
	public void unregisterForUpdate(Consumer<UpdateEventClass> action) {
		updateActionList.remove(action);
	}
	
	/* IGameObject implementation */

	@Override
	public boolean isAlive() {
		return super.isAlive();
	}
	
	@Override
	public void die() {
		super.die();
	}
	
	@Override
	public void init() {
		super.init();
		
		// Reset state
		this.millisToMove = START_MILLIS_TO_MOVE;	
		this.direction = INITIAL_DIRECTION;
		this.changedDirection = INITIAL_DIRECTION;
		this.lastUpdateMillis = 0;
		this.cellsToNormal = 0;
		this.steps = 0;
		
		// Set the snake's body 
		clear();
		assert(elemsQu.isEmpty());
		
		// Init snake's body
		addLast(new Body(fitCoords(INITIAL_X_COORD, true), fitCoords(INITIAL_Y_COORD, false)));
		for (int i = 0; i < INITIAL_LENGTH - 1; i++) {
			grow();
		}
	}

	@Override
	public void update(long elapsedTimeMillis) {
		super.update(elapsedTimeMillis);
		lastUpdateMillis += elapsedTimeMillis;

		if (lastUpdateMillis >= millisToMove) {
			// Change direction if requested
			direction = changedDirection;
			// Remeber old tail position
			int oldTailX = getTailX();
			int oldTailY = getTailY();
			
			assert(cellsToNormal >= 0);
			// The snake is either growing or not
			if (cellsToNormal > 0) {
				cellsToNormal--;
				grow();
				updateActionList.forEach(action ->
					action.accept(new UpdateEventClass(this, oldTailX, oldTailY, true, false)));
			} else {
				tailToFront(); // Move the snake by taking its tail and move it to front
				updateActionList.forEach(action ->
					action.accept(new UpdateEventClass(this, oldTailX, oldTailY, false, false)));
			}
			lastUpdateMillis = 0; //(int) time % millisToMove
			steps++;
			
			assert(lastUpdateMillis >= 0 && lastUpdateMillis < millisToMove);
			assert(direction == changedDirection);			
		}
	}

	@Override
	public void draw() {
		super.draw();
		Body head = getLast();
		// Draw body
		actionOverBody(drawRestAction);	
		// Draw head
		gameWorld.drawImage(imgHead, head.x, head.y, true);			
	}
	
	private final Consumer<Body> drawRestAction = new Consumer<Snake.Body>() {
		@Override
		public void accept(Body t) {
			gameWorld.drawImage(imgRest, t.x, t.y, true);
		}
	};
	
	/* Methods for testing */
	public void dump() {
		StringBuilder sb = new StringBuilder();
		for (Body b : elemsQu) {
			sb.append(b).append("; ");
		}
		sb.append("Total Elems: ").append(elemsQu.size());
		sb.append("; Head at: ").append(elemsQu.getLast());
		sb.append("; Tail at: ").append(elemsQu.getFirst());
		System.err.println(sb.toString());
	}
	
	/* Inner classes and enums */
	
	enum Direction {
		LEFT, RIGHT, DOWN, UP;
	}
	
	enum State {
		NORMAL, GROW, SHRINK
	}
	
	static class Body {
		public int x;
		public int y;
		
		public Body(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		@Override
		public String toString() {
			return String.valueOf(x) + ", " + String.valueOf(y);
		}		
	}
	
	static class UpdateEventClass {
		public final Snake source;
		public final int oldTailX;
		public final int oldTailY;
		public final boolean isGrowing;
		public final boolean isStatic;
		
		public UpdateEventClass(Snake source, int oldTailX, int oldTailY, boolean isGrowing,
				boolean isStatic) {
			this.source = source;
			this.oldTailX = oldTailX;
			this.oldTailY = oldTailY;
			this.isGrowing = isGrowing;
			this.isStatic = isStatic;
		}
	}

}
