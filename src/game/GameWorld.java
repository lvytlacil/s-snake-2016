package game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.function.Consumer;

import game.KeyboardState.Key;
import game.Snake.Body;
import game.Snake.Direction;
import game.Snake.UpdateEventClass;
import jogamp.opengl.glu.nurbs.Splinespec;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * Represent the gameworld
 */
public class GameWorld {
	/* Public constants */
	public static final int X_SIZE = 15;
	public static final int Y_SIZE = 9;
	public static final int GRID_ELEMENT_SIZE = 50;
	public static final float FONT_SIZE = 24.0f * (GRID_ELEMENT_SIZE / 24.0f);
	public static final int CENTER_X = X_SIZE * GRID_ELEMENT_SIZE / 2;
	public static final int CENTER_Y = Y_SIZE * GRID_ELEMENT_SIZE / 2;
	public static final String SCORE_STR = "Skore za jablka: ";
	public static final String GAME_OVER_STR = "Zemrel jsi.";
	public static final String STEPS_TAKEN_STR = "Kroku celkem: ";
	public static final String INTRO_STR = "Vitej v hadovi.\nStiskni SPACE pro start.";
	
	// Color constants, they cannot be static, snice for some reason color methods in
	// PApplet are not static.. oh well..
	private final int color_start;
	private final int color_back;
	private final int color_cherry_change;
	private final int color_death;
	
	/* Private fields */
	private final Random rand = new Random();
	// Maps strings id's to assets (images)
	private HashMap<String, PImage> assetMap = new HashMap<>();
	// Canvas used for drawing
	private PApplet canvas;
	
	// The state of the game
	private GameState currentState;
	//private GameState nextState;
	// List of game objects excluding snake and foodmanager
	private ArrayList<GameObject> gameObjects = new ArrayList<>();
	private Snake player;
	private FoodManager foodManager;
	// The game background
	private Background background;
	/* This caches where snake's cells currently are for fast collision checking. Note, that
	 * it could be an array of boolean, but then you have to make sure, that the snake cannot
	 * cross itself (no two cells on the same position). */
	private int[][] snakeCollisionCache = new int[X_SIZE][Y_SIZE];
	private int score;
	// String used to cache info text that can be displayed on the screen on game-over etc.
	private String infotext; 
	
	/* Actions */
	
	/* Action that is used to initialize the collision cache */
	private Consumer<Body> collisionCacheInit = new Consumer<Body>() {
		@Override
		public void accept(Body t) {
			snakeCollisionCache[t.x][t.y] = 1;
		}
	};
	
	/* Action to take, when snake updates. We need to update the collision chache. */
	private Consumer<UpdateEventClass> onSnakeUpdate = new Consumer<UpdateEventClass>() {
		@Override
		public void accept(UpdateEventClass t) {
			if (!t.isStatic) {
				snakeCollisionCache[t.source.getHeadX()][t.source.getHeadY()] += 1;
			}
			if (!t.isGrowing) {
				snakeCollisionCache[t.oldTailX][t.oldTailY] -= 1;
			}
		}	
	};
	
	/*
	private void requestStateChange(GameState gameState) {
		this.nextState = gameState;
	}
	*/
	
	public GameWorld(PApplet canvas) {
		this.canvas = canvas;
		this.loadAssets();
		this.currentState = GameState.MENU;
		
		// Background
		color_start = canvas.color(0);
		color_back = canvas.color(43, 73, 43);
		color_death = canvas.color(150, 40, 40);
		color_cherry_change = canvas.color(45, 117, 82);
		
		background = new Background(this);
		background.setColor(canvas.color(color_start));

	}
	
	/* Private methods */
	
	/* Checks for collision */
	private void checkForCollisions() {
		/* The head of snake is the only "object" that can cause collision */
		int sx = player.getHeadX();
		int sy = player.getHeadY();
		
		// Check for lethal collision
		// Lethal collision is when head hits some other part of the snake, that means
		// if there is more than one snake piece in the grid at the head position.
		if (snakeCollisionCache[sx][sy] > 1) {
			die();
		}
		
		// Check for food
		Apple food = foodManager.eatAtPosition(sx, sy);
		if (food != null) {
			switch (food.getEffect()) {
			case GROW:
				player.eat(2);
				updateScore(food.getValue(), true);
				speedPlayerUp();
				break;
			case SHRINK:
				player.shrink(5);				
				updateScore(food.getValue(), true);
				speedPlayerUp();
				background.addFadeRequest(color_cherry_change, 300);
				background.addFadeRequest(color_back, 300);
			default:
				// Do nothing
			}
		}		
	}
	
	private void speedPlayerUp() {
		if (score % 100 == 0)
			player.speedUp();
	}
	
	/* Updates the score */
	private void updateScore(int value, boolean add) {
		score = add ? score + value : value;
		canvas.getSurface().setTitle(SCORE_STR + score);
	}
	
	private String createGameOverText() {
		StringBuilder sb = new StringBuilder();
		sb.append(GAME_OVER_STR).append(System.lineSeparator());
		sb.append(SCORE_STR).append(score).append(System.lineSeparator());
		sb.append(STEPS_TAKEN_STR).append(player.getStepsTaken());
		return sb.toString();
	}
	
	/**
	 * Gets a random free position in the grid or null if such position could not be found.
	 * @return
	 */
	public IntPair getRandomFreePosition() {
		final int maxTries = 5000;
		int tries = 0, x, y;
		// Try to randomly find unoccupied position first
		do {
			x = rand.nextInt(X_SIZE);
			y = rand.nextInt(Y_SIZE);
			if (snakeCollisionCache[x][y] == 0 && !foodManager.isOnGridPosition(x, y)) {
				//System.err.println("Tries: " + tries);
				return new IntPair(x, y);
			}
			tries++;
		} while (tries < maxTries);
		// No luck? Take the last generated coords and just scan linearly
		for (int i = 0; i < X_SIZE; i++) {
			for (int j = 0; j < Y_SIZE; j++) {
				if (snakeCollisionCache[x][y] == 0 && !foodManager.isOnGridPosition(x, y)) {
					//System.err.println("Tries: " + tries);
					return new IntPair(x, y);
				}
				tries++;
				y = (y + 1) % Y_SIZE;
			}
			x = (x + 1) % X_SIZE;
		}		
		return null;
	}
	
	/**
	 * Adds the given gameobject to the collection.
	 * @param gameObject
	 */
	public void insertGameObject(GameObject gameObject) {
		gameObjects.add(gameObject);
	}
	
	/**
	 * Return the PImage associated with the given name or null, if such
	 * name is not associated with any asset.
	 * @param assetName
	 * @return
	 */
	public PImage getAsset(String assetName) {
		assert(assetMap != null);
		return assetMap.get(assetName);
	}
	
	/* Loads the given asset and fits it into the grid */
	private void loadAndFitAsset(String assetName, String filename) {
		PImage img = canvas.loadImage(filename);
		img.resize(GRID_ELEMENT_SIZE, GRID_ELEMENT_SIZE);
		assetMap.put(assetName, img);
	}
	
	private void loadAssets() {
		/* Assets loading */
		
		/* Assets that should fit the grid */
		loadAndFitAsset("snakeHead", "snakeHead.png");
		loadAndFitAsset("snakeRest", "snakeRest.png");
		loadAndFitAsset("apple", "apple.png");
		loadAndFitAsset("cherry", "cherry.png");
	}
	

	private void init() {
		// Background
		background.setColor(color_start);
		background.addFadeRequest(color_back, 750);
		
		// Score
		updateScore(0, false);
	
		// Player and collision cache
		player = new Snake(this);
		player.init();
		for (int i = 0; i < X_SIZE; i++)
			for (int j = 0; j < Y_SIZE; j++) {
				snakeCollisionCache[i][j] = 0;
			}
		player.actionOverCells(collisionCacheInit);
		player.registerForUpdate(onSnakeUpdate);
		
		// Food manager
		foodManager = new FoodManager(this);
		foodManager.init(this);
		foodManager.resume();
		
		// Rest of the objects
		gameObjects.clear();
		assert(gameObjects.isEmpty());
		
		for (GameObject go : gameObjects) {
			go.init();
		}
		currentState = GameState.PLAYING;
	}

    /**
     * Update method of the gameloop. This works as a simple state automaton.
     * @param lastUpdateMillis millis since last call to this method.
     */
	public void update(long lastUpdateMillis) {
		KeyboardState keyState = KeyboardState.get();
		if (currentState == GameState.MENU) {			
			if (keyState.isDown(Key.SPACE)) {
				currentState = GameState.INIT;
			}
		} else if (currentState == GameState.INIT) {
			init();			
		} else if (currentState == GameState.PLAYING) {
			// Process input
			if (keyState.isDown(Key.LEFT))
				player.changeDirection(Direction.LEFT);
			else if (keyState.isDown(Key.RIGHT))
				player.changeDirection(Direction.RIGHT);
			else if (keyState.isDown(Key.UP))
				player.changeDirection(Direction.UP);
			else if (keyState.isDown(Key.DOWN))
				player.changeDirection(Direction.DOWN);
		
			background.update(lastUpdateMillis);
			player.update(lastUpdateMillis);
			
			foodManager.update(this, lastUpdateMillis);
			for (GameObject go : gameObjects) {
				go.update(lastUpdateMillis);
			}
			// Check for collisions
			checkForCollisions();
		} else if (currentState == GameState.DYING) {
			// Wait for background to finish fading
			if (background.isEmpty()) {
				player.die();
				
				foodManager.die();
				for (GameObject obj : gameObjects) {
					obj.die();
				}
				// Cache game-over string
				infotext = createGameOverText();
				
				currentState = GameState.DEAD;
			} else {
				background.update(lastUpdateMillis);
			}
		} else if (currentState == GameState.DEAD) {			
			// Po stisku space se restartuje hra
			if (keyState.isDown(Key.SPACE)) {
				init();
			}
		}
	}

    /**
     * The draw method of the gameloop.
     * @param canvas The canvas to be used to draw the scene.
     */
	public void draw(PApplet canvas) {
		if (currentState == GameState.MENU) {
			background.draw();
			canvas.textSize(FONT_SIZE);
			canvas.textAlign(PApplet.CENTER, PApplet.CENTER);
			canvas.text(INTRO_STR, CENTER_X, CENTER_Y);
		} else if (currentState == GameState.INIT) {
			
		} else if (currentState == GameState.PLAYING) {
			background.draw();
			player.draw();
			foodManager.draw();
			for (GameObject go : gameObjects) {
				go.draw();
			}
		} else if (currentState == GameState.DYING) {
			background.draw();
			player.draw();
			foodManager.draw();
		} else if (currentState == GameState.DEAD) {
			canvas.textSize(FONT_SIZE);
			canvas.textAlign(PApplet.CENTER, PApplet.CENTER);
			canvas.text(infotext, CENTER_X, CENTER_Y);
		}		
	}
	
	/*
	public void stateChange() {
		currentState = nextState;
	}
	*/
	
	private void die() {
		background.addFadeRequest(canvas.color(color_death), 500);
		currentState = GameState.DYING;
	}
	
	public void drawImage(PImage img, float a, float b, boolean inGrid) {
		if (img != null) {
			if (inGrid) {
				a *= GRID_ELEMENT_SIZE;
				b *= GRID_ELEMENT_SIZE;
			}
			canvas.image(img, a, b);
		}
		else
			System.err.println("drawImage: Passed null img");
	}
	
	public void drawBackground(int color) {
		canvas.background(color);
	}

    /**
     * Calculates a color or colors between two color at a specific increment.
     * @param start The start color
     * @param end The end color
     * @param perc The number between 0.0 and 1.0 that indicates the interpolation amount between these two colors.
     *             Set it to 0 to get the start color and to 1 to the end color.
     * @return
     */
	public int lerpColor(int start, int end, float perc) {
		return canvas.lerpColor(start, end, perc);
	}
	
	/* Inner classes and enums */
	enum GameState {MENU, INIT, PLAYING, DYING, DEAD}
	
	/* Test methods */
	public void dumpCollisionCache() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < Y_SIZE; i++) {
			for (int j = 0; j < X_SIZE; j++) {
				sb.append(snakeCollisionCache[j][i]).append(" ");
			}
			sb.append("\n");
		}
		System.err.println(sb.toString());
	}
	
	public void dumpLinearScan() {
		int[][] a = new int[X_SIZE][Y_SIZE];
		int x = rand.nextInt(X_SIZE);
		int y = rand.nextInt(Y_SIZE);
		for (int i = 0; i < X_SIZE; i++) {
			for (int j = 0; j < Y_SIZE; j++) {
				a[i][j] += 1;
				y = (y + 1) % Y_SIZE;
			}
			x = (x + 1) % X_SIZE;
		}
		
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < Y_SIZE; i++) {
			for (int j = 0; j < X_SIZE; j++) {
				sb.append(a[j][i]).append(" ");
			}
			sb.append("\n");
		}
		System.err.println(sb.toString());
	}
}
