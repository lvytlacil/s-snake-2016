package game;

/**
 * Represents a game object with life cycle methods.
 * @author Libor
 *
 */
public class GameObject {
	private boolean initialized;
	private boolean alive;
	
	// Associated gameWorld. Use it inside init, update, draw and wherever you need.
	protected GameWorld gameWorld;
	
	/**
	 * Returns an uninitialized and dead object.
	 */
	public GameObject(GameWorld gameWorld) {
		this.initialized = false;
		this.alive = false;
		this.gameWorld = gameWorld;
	}
	
	/**
	 * Initializes this game object. After this method is finished, the object should
	 * be alive and ready to enter the game loop and update/draw can be safely called.
	 */
	public void init() {
		this.initialized = true;
		this.alive = true;
	}
	
	/**
	 * Updates the state of this object based on the elapsed time since the gameloop
	 * has last called update.
	 * @param lastUpdateMillis millis since last update
	 */
	public void update(long lastUpdateMillis) {
		if (!initialized)
			System.err.format("Error in %s: update() called before init()!.", this);
	}
	
	/**
	 * Performs drawing based on the state of this object.
	 */
	public void draw() {
		if (!initialized)
			System.err.format("Error in %s: draw() called before init()!.", this);
	}
	
	/**
	 * Lets this object die. After this method is finished, isAlive() should return
	 * false. However, the instance itself could be reused in the future by calling init again.
	 */
	public void die() {
		this.initialized = false;
		this.alive = false;
	}
	
	/**
	 * Returns true if this object is alive.
	 * @return
	 */
	public boolean isAlive() {
		return this.alive;
	}
}
