package game;

import game.KeyboardState.Key;
import processing.core.PApplet;

public class GameCanvas extends PApplet {
	/* Public constants */
	public static final long MILLIS_PER_FRAME = 33;
	public static final String GAME_NAME = "Snake";
	private GameWorld gameWorld;
	private long lastUpdateMillis;
	
	@Override
	public void setup() {		
		gameWorld = new GameWorld(this);
		
		lastUpdateMillis = Helper.currentTimeMillis();
		getSurface().setTitle(GAME_NAME);
	}

	@Override
	public void settings() {
		int width = GameWorld.X_SIZE * GameWorld.GRID_ELEMENT_SIZE;
		int height = GameWorld.Y_SIZE * GameWorld.GRID_ELEMENT_SIZE;
		size(width, height);
		
	}
	
	@Override
	public void draw() {
		//System.err.println(keyPressed);
		long currentMillis = Helper.currentTimeMillis();
		long diff = currentMillis - lastUpdateMillis;
		
		if (diff > MILLIS_PER_FRAME) {
			gameWorld.update(diff);
			lastUpdateMillis = Helper.currentTimeMillis();
			gameWorld.draw(this);
		} else
			gameWorld.draw(this);
		
		
	}
	
	@Override
	public void keyPressed() {
		KeyboardState state = KeyboardState.get();
		if (this.key == CODED) {
			if (this.keyCode == PApplet.UP)
				state.press(Key.UP);
			else if (this.keyCode == PApplet.DOWN)
				state.press(Key.DOWN);
			else if (this.keyCode == PApplet.LEFT)
				state.press(Key.LEFT);
			else if (this.keyCode == PApplet.RIGHT)
				state.press(Key.RIGHT);
		} else {
			if (this.key == ' ')
				state.press(Key.SPACE);
		}
	}
	
	@Override
	public void keyReleased() {
		KeyboardState state = KeyboardState.get();
		if (this.key == CODED) {
			if (this.keyCode == PApplet.UP)
				state.release(Key.UP);
			else if (this.keyCode == PApplet.DOWN)
				state.release(Key.DOWN);
			else if (this.keyCode == PApplet.LEFT)
				state.release(Key.LEFT);
			else if (this.keyCode == PApplet.RIGHT)
				state.release(Key.RIGHT);
		} else {
			if (this.key == ' ')
				state.release(Key.SPACE);
		}
	}
	
	// Run this project as Java application and this
    // method will launch the sketch
    public static void main(String[] args) {
        String[] a = {"MAIN"};
        PApplet.runSketch( a, new GameCanvas());
    }
}
