package game;

import java.util.HashSet;

/**
 * Represents a keyboard state that uses a hashset to store pressed keys.
 */
public class KeyboardState {
	/* This class is a singleton */
	private static class Holder {
		private static final KeyboardState instance = new KeyboardState();
	}
	
	private KeyboardState() { }
	
	public static KeyboardState get() {
		return Holder.instance;
	}
	
	enum Key {UP, DOWN, LEFT, RIGHT, SPACE, NONE}
	
	private HashSet<Key> keyDown = new HashSet<>();
	private HashSet<Key> keyRead = new HashSet<>();
	
	public void press(Key key) {
		keyDown.add(key);
		keyRead.add(key);
	}
	
	public void release(Key key) {
		
		keyDown.remove(key);
		keyRead.remove(key);
	}
	
	public boolean isDown(Key key) {
		return keyDown.contains(key);
	}
	
	public boolean isUp(Key key) {
		return !keyDown.contains(key);
	}
	
	public boolean hasBeenPressed(Key key) {
		if (keyRead.contains(key)) {
			assert(keyDown.contains(key));
			keyRead.remove(key);
			return true;
		} else
			return false;
	}
	
	public void dumpDownKeys() {
		System.err.println(keyDown);
	}

}
