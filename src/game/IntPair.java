package game;

/**
 * Represents a simple int pair class that has its default equals semantics redefined, so that
 * two two IntPair objects equal iff their respective coords equal.
 */
public class IntPair {
	public final int x;
	public final int y;
	
	public IntPair(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IntPair) {
			IntPair other = (IntPair) obj;
			boolean result = x == other.x && y == other.y;
			return result;
		} else
			return false;
	}
	
	@Override
	public int hashCode() {
		int result = 19;
		result = 37 * result + x;
		result = 37 * result + y;
		return result;
	}
}
