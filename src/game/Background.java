package game;

import java.util.LinkedList;

/**
 * Represents background that allows for simple fading effects - changing color from start to end value
 * linearly over time.
 * @author Libor
 *
 */
public class Background {
	// The color to be drawn
	private int bgColor = 0;
	// Stores requests to be proceessed in a sequentil order
	private LinkedList<FadeRequest> requests = new LinkedList<>();
	// Parent GameWorld object
	private GameWorld gameWorld;
	
	// The currently processed request or null if none is processed at the moment
	private FadeRequest currentRequest = null;
	private long elapsedTime = 0;
	private int previousColor = 0;
	
	
	public Background(GameWorld gameWorld) {
		this.gameWorld = gameWorld;
	}
	
	public void update(long elapsedTimeMillis) {
		if (currentRequest != null) { // Keep processing current request
			elapsedTime += elapsedTimeMillis;
			float perc = (float) elapsedTime / currentRequest.millis;
			bgColor = gameWorld.lerpColor(previousColor, currentRequest.color, perc);
			if (perc >= 1.0f) {
				// Fading completed
				assert(bgColor == currentRequest.color);
				currentRequest = null;
			}
			
		} else if (!requests.isEmpty()) { // New request is available
			FadeRequest req = requests.removeFirst();
			if (req.millis == 0) {
				bgColor = req.color;
			} else {
				currentRequest = req;
				elapsedTime = 0;
				previousColor = bgColor;
			}
		}
	}
	
	public void draw() {
		gameWorld.drawBackground(bgColor);
	}
	
	/**
	 * After all active requests are finished, it sets the color to the passed
	 * value immediately.
	 * @param color
	 */
	public void setColor(int color) {
		requests.add(new FadeRequest(color, 0));
	}
	
	/**
	 * Adds a request to fade into the specific color over the given time.
	 * @param color
	 * @param millis
	 */
	public void addFadeRequest(int color, int millis) {
		requests.add(new FadeRequest(color, millis));
	}
	
	public boolean isEmpty() {
		return requests.isEmpty() && currentRequest == null;
	}
	
	private static class FadeRequest {
		public final int color;
		public final long millis;
		
		public FadeRequest(int color, int millis) {
			this.color = color;
			this.millis = millis;
		}
	}

}
