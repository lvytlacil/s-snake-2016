package game;

public class Helper {
	public static final long NANOS_IN_MILLIS = 1000000L;
    public static long currentTimeMillis() {
        return System.nanoTime() / NANOS_IN_MILLIS;
    }
}
